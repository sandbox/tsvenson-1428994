<?php
/**
 * @file
 * mss_media_1.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mss_media_1_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
