<?php
/**
 * @file
 * mss_media_1.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function mss_media_1_filter_default_formats() {
  $formats = array();

  // Exported format: CKEditor
  $formats['ckeditor'] = array(
    'format' => 'ckeditor',
    'name' => 'CKEditor',
    'cache' => '1',
    'status' => '1',
    'weight' => '-10',
    'filters' => array(
      'media_filter' => array(
        'weight' => '2',
        'status' => '1',
        'settings' => array(),
      ),
    ),
  );

  // Exported format: TinyMCE
  $formats['tinymce'] = array(
    'format' => 'tinymce',
    'name' => 'TinyMCE',
    'cache' => '1',
    'status' => '1',
    'weight' => '-9',
    'filters' => array(
      'media_filter' => array(
        'weight' => '2',
        'status' => '1',
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
